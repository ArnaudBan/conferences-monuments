# Conferences sur les monuments

Hébergé par Netlify sur le domaine https://monuments.banvillet.fr/
[![Netlify Status](https://api.netlify.com/api/v1/badges/411ef48b-14fe-431d-912e-2708416141a7/deploy-status)](https://app.netlify.com/sites/confrences-monuments/deploys)

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                   | Action                                           |
| :------------------------ | :----------------------------------------------- |
| `npm install`             | Installs dependencies                            |
| `npm run dev`             | Starts local dev server at `localhost:4321`      |
| `npm run build`           | Build your production site to `./dist/`          |
| `npm run preview`         | Preview your build locally, before deploying     |
| `npm run astro ...`       | Run CLI commands like `astro add`, `astro check` |
| `npm run astro -- --help` | Get help using the Astro CLI                     |

