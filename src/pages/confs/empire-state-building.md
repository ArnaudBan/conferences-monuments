---
title: 'Empire State Building'
lat: 40.748817
long: -73.985428
layout: ../../layouts/ConfLayout.astro

---
# Empire State Building


L'Empire State Building est un gratte-ciel de styl art déco situé dans l'arrondissement de manhattan, à new york. Il est situé dans le quartier de midtown, au 350 de la 5ᵉ avenue, entre les 33ᵉ et 34ᵉ rues. Inauguré le 1er mai 1931, il mesure 381m (443,2 avec l'antenne) et compte 102 étage.
En 2022, il est le septième immeuble de la ville de New York, mais avec sa hauteur d'antenne, il se situe derrière le One World Trade Center et la Central Park Tower. Il avait retrouvé sa première place ç la suite de l'attentat terroriste du 11 septembre 2001 qui a causé la description des tours jumelles du World Trade Centre, mails l'a reperdu en 2012 avec la construction du du One World Trade Center. L'Empire State Building a été pendant des décennies le plus haut immeuble du monde. Il tire son don de l'État de New York, The Empire State.
