---
title: 'Atomium'
lat: 50.894941
long: 4.341547
layout: ../../layouts/ConfLayout.astro

---
# Atomium

L'Atomium est un monument de Bruxelles, en Belgique, construit à l'ocasion de l'éxposition universelle de 1958 et représantant la maille conventionelle du cristal de fer (stucture cubique centrée) agrandie 165 milliards de fois. Il est situé àLaeken sur le plateau du Heysel où eut lieu cette exposition. L'Atomium a été créé par l'ingénieur André Waterkeyn et érigé par les architectes André et Jean Polak. Il est devenu, au même titre que le Maneken-Pis et la Grand-Place, un symbole de la capitale de la Belgique. Symboliquement, l'Atomium incarne l'audace d'une époque qui a voulu confronter le destin de l'humanité avec les découvertes scientifiques.
